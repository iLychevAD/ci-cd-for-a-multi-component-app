terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.9.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0"
    }
  }
}

provider "gitlab" {
    token = var.gitlab_token
}

// for creating ssh key using "tls_private_key" resource
provider tls {}
