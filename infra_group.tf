resource "gitlab_group" "infra" {
  name        = "infra"
  path        = "infra"
  description = "Infrastructure stuff"
  visibility_level = "private"

  parent_id = data.gitlab_group.root-gitlab-group.id
}
module "set_avatar" {
  source = "./tf_modules/upload_avatar"

  entity_id = gitlab_group.infra.id
  entity_type = "group"
  avatar_filename = "infra.png"
}

output "infra_group_details" {
  value = join("",
      [
        "id: ${gitlab_group.infra.id}, ",
        "path: ${gitlab_group.infra.full_path}, ",
        "web_url: ${gitlab_group.infra.web_url}"
      ]
  )
}