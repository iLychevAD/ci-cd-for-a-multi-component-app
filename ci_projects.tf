locals {
  root_domain = "${split("/", data.gitlab_group.root-gitlab-group.web_url)[2]}"
}

variable "ci_projects_list" {
  description = "CI group repositories"
  type = list(map(string))
  default = [
      {
          name = "library"
          description = "Shared GitLab ci configs"
      },
      {
          name = "oci-registry"
          description = "Holds OCI images registry"
      }
  ]
}

module "ci_project" {
  for_each = {for project in var.ci_projects_list: project.name => project }
  source = "./tf_modules/gitlab_project_with_restore_backup"

  gitlab_token = var.gitlab_token

  project_name = each.value.name
  group_id = gitlab_group.ci.id
  description = try(each.value.description, each.value.name)

  deploy_key_readonly = var.deploy_key_readonly
  deploy_key_readwrite = var.deploy_key_readwrite
}
# so apps can know the location of the shared ci configs
resource "gitlab_group_variable" "SHARED_CI_CONFIGS_LIBRARY_PROJECT" {
  group             =  data.gitlab_group.root-gitlab-group.full_path
  key               = "SHARED_CI_CONFIGS_LIBRARY_PROJECT"
  value             = module.ci_project["library"].path_with_namespace
}

# Upload avatars :)
module "set_ci_projects_avatar" {
  for_each = {for project in var.ci_projects_list: project.name => project}
  depends_on = [ module.ci_project ]
  source = "./tf_modules/upload_avatar"

  entity_id = module.ci_project[each.value.name].id
  entity_type = "project"
  avatar_filename = "${each.value.name}.png"
}

# Deploy token for OCI images
resource "gitlab_deploy_token" "oci-registry" {
  project    = module.ci_project["oci-registry"].id
  name       = "oci-registry"
  username   = "oci-registry"
  expires_at = "2023-05-01T00:11:22Z" #timeadd(timestamp(), "1000h")
  scopes = ["read_registry", "write_registry"]
}
# Write the token as a global variable
resource "gitlab_group_variable" "OCI_REGISTRY_DEPLOY_TOKEN" {
  group             =  data.gitlab_group.root-gitlab-group.full_path
  key               = "OCI_REGISTRY_DEPLOY_TOKEN"
  value             = gitlab_deploy_token.oci-registry.token
  masked = true
}
resource "gitlab_group_variable" "OCI_REGISTRY_URL" {
  group             =  data.gitlab_group.root-gitlab-group.full_path
  key               = "OCI_REGISTRY_URL"
  value             = "registry.${local.root_domain}/${module.ci_project["oci-registry"].path_with_namespace}"
}
resource "gitlab_group_variable" "ROOT_GITLAB_GROUP_NAME" {
  group             =  data.gitlab_group.root-gitlab-group.full_path
  key               = "ROOT_GITLAB_GROUP_NAME"
  value             = data.gitlab_group.root-gitlab-group.name
}

# To be able to use images from our private OCI registry
# See https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#configure-a-job
resource "gitlab_group_variable" "OCI_REGISTRY_DOCKER_AUTH_CONFIG" {
  group             =  data.gitlab_group.root-gitlab-group.full_path
  key               = "OCI_REGISTRY_DOCKER_AUTH_CONFIG"
  value             = join("", [
      "{\"auths\": {",
      "\"${gitlab_group_variable.OCI_REGISTRY_URL.value}\": {",
      "\"auth\": \"",
      base64encode("${gitlab_deploy_token.oci-registry.username}:${gitlab_deploy_token.oci-registry.token}"),
      "\"",
      "} } }"
  ])
}

### OUTPUTS
output OCI_REGISTRY_DEPLOY_TOKEN {
  value = gitlab_deploy_token.oci-registry.token
  sensitive = true

}
output OCI_REGISTRY_URL {
  value = gitlab_group_variable.OCI_REGISTRY_URL.value
  sensitive = true
}
output OCI_REGISTRY_DOCKER_AUTH_CONFIG {
  value = gitlab_group_variable.OCI_REGISTRY_DOCKER_AUTH_CONFIG.value
  sensitive = true
}
