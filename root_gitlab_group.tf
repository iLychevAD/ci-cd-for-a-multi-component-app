# GitLab, WTF ? https://gitlab.com/gitlab-org/gitlab/-/issues/244345
data "gitlab_group" "root-gitlab-group" {
  # to be created manually in web UI
  group_id = var.root_gitlab_group_id
}
