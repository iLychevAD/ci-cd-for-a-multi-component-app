resource "gitlab_group" "apps" {
  name        = "apps"
  path        = "apps"
  description = "Applications"
  visibility_level = "private"

  parent_id = data.gitlab_group.root-gitlab-group.id
}
resource "gitlab_group_variable" "APPS_SHARED_CI_CONFIG_NAME" {
  group             = gitlab_group.apps.full_path
  key               = "APPS_SHARED_CI_CONFIG_NAME"
  value             = "apps/ci.yml"
}
module "set_apps_group_avatar" {
  source = "./tf_modules/upload_avatar"

  entity_id = gitlab_group.apps.id
  entity_type = "group"
  avatar_filename = "apps.png"
}

locals {
  apps_group_details = join("",
      [
        "id: ${gitlab_group.apps.id}, ",
        "path: ${gitlab_group.apps.full_path}, ",
        "web_url: ${gitlab_group.apps.web_url}"
      ]
  )
}
output "apps_group_details" {
  value = local.apps_group_details
}