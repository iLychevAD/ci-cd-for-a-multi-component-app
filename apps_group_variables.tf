// use this file to set GitLab group variables

/* 
variable "variables_list" {
  type = list(map(string))
  default = [
      {
          key = "APPS_GROUP_SECRET_VAR1",
          val = "some-SECRET-value",
          masked = true
          protected = false
      },
      {
          key = "APPS_GROUP_VAR3",
          val = "some value"
      }
  ]
}
*/

/* resource "gitlab_group_variable" "variable" {
  for_each =         {for gitlab_var in var.variables_list: gitlab_var.key => gitlab_var } #var.variables_list
  group             = gitlab_group.apps.id # The same for each
  key               = each.value.key
  value             = each.value.val
  protected         = try(each.value.protected, false)
  masked            = try(each.value.masked, false)
  #environment_scope = try(each.value.environment_scope, false)
}
*/

