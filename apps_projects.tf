variable "apps_list" {
  description = "List of all application repositories under Apps group"
  type = list(map(string))
  default = [
      {
          name = "app1"
          description = "Some application"
      },
      {
          name = "app2"
      },
      {
          name = "app3"
          description = "Some application number 3"
      }
  ]
}

module "apps_project" {
  for_each =         {for project in var.apps_list: project.name => project }
  source = "./tf_modules/gitlab_project_with_restore_backup"

  gitlab_token = var.gitlab_token

  project_name = each.value.name
  group_id = gitlab_group.apps.id
  description = try(each.value.description,"No description for the ${each.value.name}")
  /* if use ci config from a remote repo 
  (to prevent application developers with write access to an application repo to modify the pipeline definition) 
  then the GitLab Pipeline Editor's View Merged YAML feature doesnt work :(
  So, for better UX for this demo we instead have to put its own .ci-gitlab.yaml into each repo which just contains 
  include of the remote ci config:
   ---
   include:
     - file: "apps/ci.yml"
       project: "<path to remote project>"

  ci_config_path = "apps/ci.yml@${module.ci_project["library"].path_with_namespace}:main"
  */
  deploy_key_readonly = var.deploy_key_readonly
  deploy_key_readwrite = var.deploy_key_readwrite
}

output "apps_projects_properties" {
  value = {
    for project in var.apps_list: project.name => join("", [
      "id: ${module.apps_project[project.name].id}, ",
      "path: ${module.apps_project[project.name].path_with_namespace} , ",
      "ssh: ${module.apps_project[project.name].ssh_path}"
      ])
    }
}
