This repo manages environments (`envs` for brevity) lifecycle - creation, updating, deletion.

To create a new env:

1. Push an empty branch named as `env/<env name>` (env name should be DNS names compatible, i.e. alphanumeric and hyphens and underscores)

2. Create a MR to this branch with correct files (see below).


Upon MR the ci will check correctness of the files and permissions and generate Terrafrom plan if everything OK.

After that the author of the MR can merge it to trigger applying the Terraform plan thus updating an environment.

#### Files to configure an env
(Note: this is mostly an abstract reasoning which doesnt claim to be the best solution. Instead it is just a description of a most simplistic approach that would certainly require to be thought more scrupulously in real application).

You need to put into newly created `env/<env name>` branch at least one file named `blueprints-version.yml`(the file format doesnt matter, anyway the file is to be read by your custom script and can be anything).

It specifies the version of the content of the `environment-blueprints` repo, i.e. which version of the IaC files the infrastructure for an env should be created from. The version can be implemented as Git tags.
Upon a MR check the CI configuration fetches the specified version of the `environment-blueprints` and generates `terraform plan` for the environment using it. The plan is saved as an artifact and displayed on the MR page. Upon MR merge the corresponding plan artifact is taken from the artifacts repo and applied through `terraform apply`. So if a new infrastructure blueprints version has came up and you'd like to update the environment to it you just need to adjust the version (Git tag in our example) in the `blueprints-version.yml`. Yes, it's basically the same how Terraform modules version specification works and instead making up self-made custom approach with specifiying version we could just leverage the Terraform standard functionality, but given that in the `environment-blueprints` repo we have already used modules that would effectively add the second layer of module referencing and would be cumbersome. So, this is why a custom approach is used.

Also some additional files can be present:

If you have a lot of components you would most like do not want all of them present within a particular environment especially if such an env is purposed to test only a part of the whole system. For that reason you can create a file like `enabled-apps.yml` containing Terraform variables in a form of `appX_enabled = 1` or `appN_enabled = 0`. Then for each module in the main.tf file in the `environment-blueprints` repo use `count` meta-argument [link](https://www.terraform.io/language/meta-arguments/count) to figure out if a module should be included or not .

If you want to limit the set of users allowed to deploy applications to an env you can put here something like `users.yml` inside which list the allowed users. This file would then be checked within deployment stage in an application CI config to figure out if a user can deploy. The same file can be used to check (within a MR) if auser is allowed to modify the infrastructure for an env.


#### Env deletion
The exact approach to delete env may vary depending on your requirements. For example you might leverage GitLab's env autostop on branch deletion feature (https://docs.gitlab.com/ee/ci/environments/index.html#stop-an-environment-when-a-branch-is-deleted) if you dont care about deleted env. Or if you dont want to delete an env at all immediately you may use some indicator (for example, a separate file like DELETED.txt or a record inside env settings file to signify that an env should be deleted but the branch should be left in place for further investigation for example).

Another idea - indicate a desire to delete an env (without immediately deleting a corresponding branch) by creating a MR like `deleted-env/<env name> --> env/<env name>`. CI then checks if a MR author elligible to delete an env if yes conducts deletion procedure and changes the prefix of the branch to `deleted-env` (the original branch is deleted then).

Purging of old branches with `deleted-env` prefix can be implemented as a scheduled job.

