# This is a fake module that pretends to contain logic
# to create a k8s cluster

resource "null_resource" "k8s" {
  triggers = {
    #always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo Kubernetes cluster processing"
    interpreter = ["sh", "-c"]
  }
}

output control_plane_url {
  value = "https://some:8080"
}

output ingress_url {
  value = "https://some"
}
