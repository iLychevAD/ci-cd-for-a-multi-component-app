# This is a fake module that pretends to contain logic
# to create a VPC (or fetch information of existing "shared" VPC) and corresponding network resources and return key values 
# to be consumed by other modules
resource "null_resource" "vpc" {
  triggers = {
    #always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo VPC processing finished !"
    interpreter = ["sh", "-c"]
  }
}

output id {
  value = "123"
}
output cidr_block {
  value = "10.1.0.0/16"
}
output subnets {
  value = ["123", "456", "789"]
}
