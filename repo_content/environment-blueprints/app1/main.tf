# App1
resource "null_resource" "rds" {
  triggers = {
    #always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo RDS for app1"
    interpreter = ["sh", "-c"]
  }
}

output rds_host {
  value = "app1-db.aws.com:3306"
}
