# This is a fake module that pretends to contain logic
# to create resources requested by the App 2
resource "null_resource" "s3" {
  triggers = {
    #always = timestamp()
  }
  provisioner "local-exec" {
    command = "echo Some changes for S3 for App 3"
    interpreter = ["sh", "-c"]
  }
}

output name {
  value = "bucket-app3"
}

