variable own_vpc {
  default = true
  description = "If create its own VPC or use a shared one ?"
}
variable k8s_enabled {
  default = true
  description = "If create k8s ?"
}

variable app1_enabled {
  default = false
  description = "If create resorces for app1 ?"
}
variable app2_enabled {
  default = false
  description = "If create resorces for app2 ?"
}

