terraform {
  backend "http" {
  }
}

module vpc {
  source = "./vpc"
}

module k8s {
  source = "./k8s"
}

module app1 {
  source = "./app1"  
}

module app2 {
  source = "./app2"  
}

module app3 {
  source = "./app3"  
}
