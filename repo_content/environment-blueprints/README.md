Generic IaC templates to be used by environments.

The template sets are supposed to be versioned using Git tags so can be referred to from the environment configuration files to indicate which version of the infrastructure should be used for an environment.

