import os
import yaml
from pprint import pprint as pp

DEPLOY_CI_CFG_NAME = os.environ.get('DEPLOY_CI_CFG_NAME', '')
ENVS_TO_DEPLOY = os.environ.get('ENVS_TO_DEPLOY', '')

envs = os.environ.get('ENVS_WITH_APP', '').strip("'").split()
print(f'the envs is {envs}')

when = 'manual'

if ENVS_TO_DEPLOY:
    envs = ENVS_TO_DEPLOY.strip("'").split()
    when = 'on_success'

ci_cfg_content = {}
ci_cfg_content['stages'] = [
    'fetch-ci-library',
    'deploy-prod',
    'deploy-nonprod'
]
stage = ''
needs = []

for env in envs:
    print(env)
    stage = 'deploy-prod' if env == 'prod' else 'deploy-nonprod'
    deployment_tier = 'production' if env == 'prod' else 'staging'
    needs = [] if env == 'prod' else ['_']
    needs.append('fetch-ci-library')
    #needs.append({
    #    'pipeline': os.environ.get('CI_PIPELINE_ID'),
    #    'job': '_generate-deploy-ci-config'
    #})
#    ci_cfg_content[env] = {
#        'stage': stage,
#        'when': 'manual',
#        'environment': {
#            'name': env,
#            'deployment_tier': deployment_tier
#        }
#    }

    ci_cfg_content[env] = {
        'stage': stage,
        'needs': needs,
        # If envs list has been provided
        # automatically deploy to the envs
        'rules': [
            {
                'when': when
            }
        ],
        'variables': {
            'ENV': env
        },
        'trigger': {
            'include': [
                {'artifact': 'deploy.yml',
                'job': 'fetch-ci-library'}
            ],
            'strategy': 'depend'
        }
    }

# This is a nasty hack to disable 'play all' button
# The button disappears if at least one stage has run
ci_cfg_content['_'] = {
    'stage': 'deploy-nonprod',
    'when': 'always',
    'script': [ 'printenv', 'ls -lah' ],
    'needs': {
        'pipeline': os.environ.get('CI_PIPELINE_ID'),
        'job': '_generate-deploy-ci-config'
    }
}
# Fetch ci library repo content to get the deploy.yml file
ci_cfg_content['fetch-ci-library'] = {
    'stage': 'fetch-ci-library',
    'when': 'always',
    'image': 'python:3.8',
    'script': [
        'git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}/${SHARED_CI_CONFIGS_LIBRARY_PROJECT}.git ci_library',
        'cp ci_library/apps/deploy.yml deploy.yml'
    ],
    'artifacts': {
        'paths': [
            'deploy.yml'
        ]
    }
}

print(yaml.dump(ci_cfg_content))

with open(DEPLOY_CI_CFG_NAME, 'w') as f:
    yaml.dump(ci_cfg_content, f)

