Shared ci configs and bash scripts to be used by various projects:

### Structure:

```
apps
|
|_ci.yaml <-- intended to be used by all projects in the `apps` group as a default ci config
|
|_deploy-ci-helper.py <<-- generates Gitlab child pipeline config for deployments
|
|_deploy.yml <<-- ci config for a deployment to an environment 
|
|_<app name dir> <-- to overwrite the default ci config when required

infra
|
|_environment-set.yml <<-- ci config for the environment-set repo

```
