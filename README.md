This demo overviews a case of migrating a ci/cd structure for an existing multi-component application from an discontinued Atlassian Bamboo Server ([link](https://www.atlassian.com/migration/assess/journey-to-cloud)) to [GitLab CI](https://docs.gitlab.com/ee/index.html) (Comminity Edition).

First you will find a description of the current state of affairs - i.e. how the ci/cd has been organized within Bamboo Server, how the Bamboo Build and Deploy plans are designed for bootstrapping infrastructure and deploying the components of the application, what is in general the architecture of the application itself.

And in the second half of this README you will see a proposal on imlementing the same picture leveraging the virtues of `GitLab CI`.

## Initial state

(Note: this is not a description of some particular project but more a kind of compilation of several projects I worked on).

The application solution allows the client to fulfill a particular business purpose (the nature of which is not relevant here and thus not specified) and consists of more than 50 discrete components (further refered to as `applications` or just `apps` or `components`). I refrain from calling them microservices as each of them looks more like a full-fleged application communicating with other siblings using REST API and messages in Kafka topics. Some of them expose a web UI to external or internal users, some are just utility parts serving needs of other components or performing internal operations, etc.

Code for each app is stored in its own Git repository (further just `repo`). So, a `multi-repo` approach is used for them. Each app may be written in different languages and packaged as one or several OCI-images for deployment.

Each app repo looks like:
```
📦 <some-app-git-repo>
 ┣ 📂src <-- application source code
 ┣ 📂docker-compose
 ┃ ┗ 📜docker-compose.yml <-- analogue of K8s manifests
 ┗ 📜Dockerfile <-- conventionally, "Dockerfile" name is used for OCI image specification file
```

For running the applications the client uses an outdated orchestration system (one from pre-Kubernetes epoch). So each app repo contains a docker-compose compatible file describing deployment directives for that outdated orchestration system (in essence similar to Kubernetes Deployment manifests). 

For all of the build and deploy activities Attlassian Bamboo Server is used. 

Some details for those not familiar with the Bamboo Server - in an opinionated manner it explicitly separates so called `build` pipelines and `deployment` pipelines. The former are supposed to build application code and produce some artifacts for further deployment (in our case those artifacts are OCI images uploaded to OCI registry and docker-compose.yml files referring to those images). The latter ones are supposed to take some particular set of artifacts and apply it to some particular `environment`. An `environment` (further `env` for brevity) here is just an abstract deployment target characterized by a set of environment variables attached to it and exposed to the apps deployed into it. In reality an `env` is implemented as a set of resources (virtual machines, databases, object storage locations etc) required by the applications.

In Bamboo one `build` pipeline usually corresponds to one `deployment` pipeline so when the latter one is started it just takes the artifacts from the attached `build` pipeline as input. 

The client uses a `production` env, `preproduction` env and numerous (up to several hundreds) so called `staging` (short-lived) envs where different development teams and software engineers can test various combinations of the apps (here we assume that they have ~80-100 distinguish components of the application solution and several hundreds of software developers which gives a lot of possible combinations and requires so many `staging` envs).

Roughly, a configuration of a `deploy` pipeline consists of a specification of the source artifacts (which are provided by the attached `build` pipeline as described earlier) and a specification of the set of envs where those artifacts (effectively, an application) can be deployed to.

Current installation uses sophisticated dynamic generation of envs set for each app deployment pipeline. Roughly speaking, they have a central configuration file with the list of all existing envs where for each env a list of apps allowed to be deployed to it is denoted. Each time the file is modified (i.e., an env is created or deleted) the deployment pipelines are automatically being updated so as in the result each of them contains a list of envs corresponding for each app. You will have more idea about this aspect when you have looked at the implementation section later.


<details>
<summary>In the Bamboo Server specific YAML format the deploy pipeline looks like this (some keywords are omitted for brevity, so consider this as a pseudocode, this is not a valid Bamboo config):</summary>

```
---
deployment:
  name: Some Application deployment pipeline
environments:
- preproduction
- preproduction
- staging-1

production:
  tasks:
  - clean
  - artifact-download
  - command:
      executable: deploy
      argument: '-f docker-compose.yml'
  variables:
    APP_ENV: production
    JAVA_OPTS: ''
    LOG_DIR: /logs/prod

preproduction:
  tasks:
  - clean
  - artifact-download
  - command:
      executable: deploy
      argument: '-f docker-compose.yml'
  variables:
    APP_ENV: preproduction
    JAVA_OPTS: ''
    LOG_DIR: /logs/preproduction

staging-1:
  tasks:
  - clean
  - artifact-download
  - command:
      executable: deploy
      argument: '-f docker-compose.yml'
  variables:
    APP_ENV: staging-1
    JAVA_OPTS: ''
    LOG_DIR: /logs/staging-1
```
</details>

.

<details>
<summary>In the Bamboo UI this looks like:</summary>

![a link](pics/envs_list_on_build_result_page.png)

</details>

Here you can see an application build result page where on the right-hand side under the `Included in deployment project ` title you can see a list of envs into which you can deploy the application. (Keep in mind that besides `build` and `deployment` pipelines the Bamboo also uses a notion of `releases` - this is just a some kind of an intermediate entity that should be created out of a build result to make possible to deploy that build into some env). The `cloud-with-upwards-arrow` button in the `Actions` column starts a corresponding `deploy` pipeline with automatically passing the link to a build result (in a form of a `release` entity in Bamboo terminology) and the name of the env next to which the button has been clicked (the procedure of how a list of envs is created for a `deploy` pipe is described earlier).

A concept of a `release` is specific to Bamboo Server though provides some amenities. For example on the Release details page you can see a list of envs where a release has been deployed to. On the `Commits` tab you can backtrack a release to the application code in a SVC. And `Issues` tab shows attached Jira tickets.
<details>
<summary>Release details page</summary>

![a link](pics/bamboo_release_details.png)

</details>

.

As well as an env details page enumerates releases history for this env (in scope of one particular application though as an env is specified for each deployment pipeline individually):
<details>
<summary>Env details page</summary>

![a link](pics/bamboo_env_details.png)

</details>

.

And upon clicking the `cloud-with-upwards-arrow` button the Bamboo shows diff of Jira tickets and commits in respect to the previous `release` (only if both releases are made from artifacts from the same Git branch):
<details>
<summary>Deploy launch page</summary>

![a link](pics/deploy_launch_page.png)

</details>

.

So, in general, current path from source control to an env for each app looks like:
```
Git repo --> Build pipeline --> artifacts --> Deploy pipeline |--> Prod env
                                                              |
                                                              |--> some nonprod env 1
                                                              |
                                                              | ...
                                                              |
                                                              |--> some nonprod env N
```

The Build plans are triggered automatically upon Git commits or Git tags. Most of the Deployment plans are started by the project members manually when needed. Each Deploy plan contains a step that checks if a user started the plan has permissions to deploy into an env (for example, only members of the team which owns an env are allowed to deploy to that env and the deployment to the production env is allowed only for a set of elligible project members).

## The task

The task is to migrate the aforementioned design from Bamboo Server to `GitLab` while keeping a similar deployment scheme (leveraging GitLab's `Environments` feature).
Also the following should be considered:

 - team members (software engineers, quality assurance specialists) are supposed to be able to manage environments on their own in a user-friendly self-service manner.
 - there should not be any discrepancy in IaC for different environments (per `12-factor apps` best practices), i.e. for any kind of an environment, be it a development or production one, the same set of IaC (here - Terraform files) should be used.
  - the core ideas and workflows established in the previous situation (implemented with Atlassian Bamboo) should be kept to make the migration more smooth for the members of the projects (also sometimes referred to as just users) 

## Implementation

### Implementation's GitLab groups\projects structure

```
📦 <GitLab root group>
 ┣ 📂 apps GitLab group
 ┃ ┣ 📃 app1 GitLab project
 ┃ ┣  ...
 ┃ ┗ 📃 appN GitLab project
 ┣ 📂 ci GitLab group
 ┃ ┣ 📃 library GitLab project
 ┃ ┗ 📃 oci-registry GitLab project
 ┗ 📂 infra GitLab group
  ┣ 📃 environment-blueprints GitLab project
  ┣ 📃 environment-set GitLab project
  ┗ 📃 k8s-gitops GitLab project
```

*Description*:

The most important content is in the `ci/library` repo (the shared ci configs) and `environment-set` repo. The other repos dont require much attention: the `k8s-gitops` purpose is not implemented and the repo is empty, the `apps` group just imitates source code for some apps, `ci/oci-registry` serves a role of a OCI registry for the solution.

The `apps` GitLab group merely contains the apps source code per se. Each GitLab project in this group corresponds to one app. Each app repo is expected to contain the source code itself (in the `src` directory for example), a `k8s` directory with k8s manifests and an OCI image specification file (traditionally often called `Dockerfile`). 

The `ci` GitLab group contains the `ci/library` project that holds shared `.gitlab-ci.yaml` files used by other projects (in a manner similar to Jenkins' shared libraries) and the `ci/oci-registry` - its purpose is to serve as a OCI-image registry for various images used by the demo project (it also contains a Git repository with gitlab-ci files to build some utility images with tools used in various pipelines). For simplicity the latter stores all the images throughout all the projects of the demo though it's clearly not the best choice for a real life situation where for different sets of images a separate projects/registries should be created.

The `infra` group holds applications infrastructure creation related Git repositories:

The `infra/k8s-gitops` is mostly irrelevant to the topic of this demo. In this demo it's supposed that Kubernetes is used as a computation workload platform and when a k8s cluster is created for an environment all the k8s manifests are supposed to be put into this repo (where each branch corresponds to a single environment) to be consumed by a GitOps tool installed into the cluster.

The `infra/environment-blueprints` holds parametrized IaC templates describing all the resources required for a full-fleged environment. In this example the Terraform is used as a IaC tool though the principles are similar for its analogues, for instance, CloudFormation. The blueprints are parametrized in such manner that in the defaults values they hold some sensible values (most likely set to different values depending on the kind of a environment they were used to bootstrap - for examle, a production env and everything else). It's implied that there might coexist several versions of the blueprints (implemeted by using Git branches or Git tags) so each environment (see the next paragraph about `infra/environment-set`) can explicitly specify which version it wants to use (in case of using Terraform by specifiying Git reference in the module's `source` field).

Here I would like once again highlight a digression from the best practices. For simplicity in the `infra/environment-blueprints` repo all the parts of an environment are combined into one single Terraform module (or a workspace, or a Stack in CloudFormation's terminilogy) in that sense that all the resource are always updated or changed within a single `terraform apply` command. Which is cumbersome and doubtly feasible for large infrastructures containing a lot of resources. For the latter ones it would be more manageable to split into disparate Terraform modules (or CloudFormation Stacks, or Azure ARM Resource Groups) and thus make it possible for the infrastructure to be changed/updated in parts according to which exact parts have changed. This albeit might raise another question - how to manage dependencies in between such parts if they are present ? For that we would use some kind of an external (in respect to the IaC tool itself) orchestration tool like AWS Step Functions ... or even GitLab's DAG feature!

Finally, the `infra/environment-set` project represents an actual expected state of resources for each environment (a branch corresponds to an environment). See the README.md file in the Git repo for details. In short, each branch here is meant to contain a `main.tf` file referring to some version of the blueprints in the `infra/environment-blueprints` project, a set of Terraform files with overrides for any default variables set in the blueprints modules and other utility files like with a list of users allowed to deploy to the environment (such a list is to be checked by the deployments job in the apps projects).

### *Important!*

While looking at the implementation keep im mind that this solution deliberately omits some crucial aspects of any project infrastructure like security or monitoring. Just for the sake of keeping this solution manageable and comprehensible. Implementing security and monitoring aspects would make the solution cumbersome and much longer to prepare. The same concerns the `k8s-gitops` repository - it's implied that in a real life solution this would actively participate in the deployment process and hold Kubernetes clusters state in a GitOps approach but currently this repo is just a placeholder. In the practical guide later you will see a description of the process of controlling environments using different branches in the `infra/environment-set` project. Ideally such a workflow should use Merge Requests though for simplicity this implementation skips using MRs.

Another important thing that's obscure in this solution is configuration management, i.e. how configuration settings unique to each environment are provided to the applications inside an environment. Well, given that our applications run within Kubernetes cluster and that's cluster state is placed into a dedicated repo (`k8s-gitops` in our case), for the configuration settings the situation is simple - for each app the Terraform files in the `infra/environment-blueprints` should output all the sensible configuration values for the resources (like S3 bucket names, RDS endpoint URLs etc). Then using the Terraform itself or some other tool in the end of creating/updating an environment an additional step should collect all those outputs, transform them into k8s ConfigMap manifests and put them into the GitOps repo. 

For the secrets we can go several ways. Most simplistic (though not flexible and not easy for secrets rotation) way - is to use some kind encryption at rest like Mozilla's SOPS so that the secrets are being encrypted when they are put into GitOps repo and decrypted when deployed into K8s. Another way (and better ?) way - do not store secrets at rest at all but use either 3rd-party tool like Hashicorp Vault (with dynamic secrets generation) or cloud native features like [AWS IAM Roles for Service Accounts](https://aws.amazon.com/blogs/containers/diving-into-iam-roles-for-service-accounts/).


## Bootstrap the demo

This project contains Terraform files that enable you to install a copy of the demo structure into your own GitLab account to see it in action:

`*.tf` files in the root directory and in the `tf_modules` directory describe the structure and configuration of the GitLab projects and groups. In the `repo_content` directory there is a content for the GitLab repositories in the projects. The repositories are filled with those files by the Terraform scripts.

The demo was tested with GitLab Community Edition `15.0.0-pre revision 4bda1cc84df`. The Terraform scripts do not create any real resources but just imitate them using `null_resource` and `local-exec`.

The bootstrapping process is conducted inside a container image (see the steps below) so it's a platform-agnostic and in terms of tools all you need to spin up the demo is some containerization engine installed on your PC (i.e., Docker, Podman, etc).

*Steps*:

1. In the GitLab web UI manually create a root group to bootstrap the demo into (see `root_gitlab_group.tf` for a web-link why it's not possible to automate). Notice its ID - you need to provide it at the next step.

2. Clone this repository.
Download an official Hashicorp's Terraform image and enter its interactive shell. All the further commands are supposed to be performed inside that shell:

```
docker run --rm -it --name ci-cd-for-a-multi-component-app \
  -e TF_VAR_gitlab_token=<your GitLab account access token> \
  -v <path to a location where to store ssh key-pairs on your PC>:/deploy-keys \
  -e TF_VAR_deploy_key_readwrite=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key.pub \
  -e TF_VAR_deploy_key_readonly=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key.pub \
  -e TF_VAR_root_gitlab_group_id=<GitLab group ID> \
  -e GITLAB_BASE_URL=https://gitlab.com/api/v4/ \
  -e GITLAB_SSH_URL=gitlab.com \
  -v <path to the directory where you cloned the project into>:/repo -w /repo \
  --entrypoint /bin/sh \
  public.ecr.aws/hashicorp/terraform:1.1.9
```

Explanation:

` -e TF_VAR_gitlab_token=<your GitLab account access token>` - Terraform's `gitlab` provider needs a GitLab access token with sufficient permissions to spin up the demo. Provide it as a Bash environment variable - `TF_VAR_gitlab_token` (see `provider.tf`). It is also used by the `upload_avatar` module.

` -v <path to a location where to store ssh key-pairs on your PC>:/deploy-keys` - on the left-hand side here specify some directory on your local PC where you would like to store SSH keys needed for deploying the demo. Thus they are persisted even if you exit the container. See bullet point `4` for more details.

` -e TF_VAR_deploy_key_readwrite=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key` and

` -e TF_VAR_deploy_key_readonly=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key` - set the names for the aforementioned keys

` -e GITLAB_BASE_URL=https://gitlab.com/api/v4/` - change it if you use on-prem GitLab installation

` -e GITLAB_SSH_URL=gitlab.com` - change it if you use on-prem GitLab installation (this is the host name used in the git clone command for ssh)

` -v <path to the directory where you cloned the project into>:/repo -w /repo` - we mount the project content from your local PC into the running container. Note that because of that the Terraform local state file will be stored inside that directory on your PC.

3. Install tools - bash and curl:

```
apk add bash curl

/bin/bash
```

4. Upon bootstrapping the demo the repositories' content is pushed into (i.e. is restored) from the `repo_content` directory. (As well as upon destroying the demo the content of the repositories is automatically pulled (i.e. is saved) into the same directory - probably you dont need this but I implemented that for my convinience during creating the demo). For that we should create a SSH key pair and need it be the same throughout both phases. In this step we generate it:

```ssh-keygen -t rsa -N '' -f /deploy-keys/ci-cd-for-a-multi-component-app-deploy-key <<< y```

```chmod 0400 /deploy-keys/ci-cd-for-a-multi-component-app-deploy-key```

A trick used in `tf_modules/gitlab_project_with_restore_backup/main.tf` requires that in the host section of the SSH public key the location of the private key is specified (in a form like `filename@~/.ssh/<filename>`). Otherwise the `tf_modules/gitlab_project_with_restore_backup` wont work. Edit accordingly:

```sed -i -e 's|^\(ssh-rsa .*\) \(.*\)$|\1 ci-cd-for-a-multi-component-app-deploy-key@/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key|' /deploy-keys/ci-cd-for-a-multi-component-app-deploy-key.pub```

Now you can proceed with bootstrapping the demo using Terraform:

Initialize Terraform by `terraform init` so it installs all the providers.

Deploy the demo with Terraform by `terraform apply`.

*Notice*: during Terraform execution you may see an error:
```
Error: POST https://gitlab.com/api/v4/projects/multi-component-app-root-group/ci/library/deploy_keys: 400 {message: {deploy_key.fingerprint_sha256: [has already been taken]}}

```
I believe this is some glitch in the GitLab API. To fix just run `terraform apply` once again until it shows no errors.

After that you should see the following structure in GitLab in the root group :

![a link](pics/gitlab_projects_tree.png)

All the projects should be filled with files from the `repo_content` directory.

Do not delete the directory with the cloned project and the files created inside it if later you would want to clean up the things. See the next section for instructions.

## Cleaning up

Launch a container image the same way you did for bootstrapping the demo (see the previous section). It's supposed that you didnt delete any files in `<path to a location where to store ssh key-pairs on your PC>` and `<path to the direcory where you cloned the project into>`: 

```
docker run --rm -it --name ci-cd-for-a-multi-component-app \
  -e TF_VAR_gitlab_token=<your GitLab account access token> \
  -v <path to a location where to store ssh key-pairs on your PC>:/deploy-keys \
  -e TF_VAR_deploy_key_readwrite=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key.pub \
  -e TF_VAR_deploy_key_readonly=/deploy-keys/ci-cd-for-a-multi-component-app-deploy-key.pub \
  -e TF_VAR_root_gitlab_group_id=<GitLab group ID> \
  -e GITLAB_BASE_URL=https://gitlab.com/api/v4/ \
  -e GITLAB_SSH_URL=gitlab.com \
  -v <path to the direcory where you cloned the project into>:/repo -w /repo \
  --entrypoint /bin/sh \
  public.ecr.aws/hashicorp/terraform:1.1.9
```

Install curl:

```apk add curl```

Do `terraform destroy`.

*Notice*: You may see some errors regarding deleting the `oci-registry` project with OCI images. In that case just delete the images and remove the project manually or wait while GitLab does that itself later.

Now if you want you can remove the cloned project directory and the `<path to a location where to store ssh key-pairs on your PC>` directory.

If you would like to deploy the demo once again without removing the directory with the cloned repo dont forget to remove files created during the previous demo deployment, namely `terraform.tfstate` files in the root directory and `.git` directories everywhere in the `repo_content` directory.

## Practical guide: a user story

OK, now you have the demo deployed and you would like to play with it to understand how it works.

Let's imagine that one of the members of our project is Jonh Dow. He is a software engineer responsible for developing some components (app1, app2 & app3) of the whole application and he and his team would like to test those components in several combinations of the versions on a several distinguish preview environments. So, what it would look like ?

First of all lets make some commits to the app1, app2 and app3 source code and get successfull builds upon those commits.
After that we should create releases for those apps to be able to deploy them (as the deployment part of the apps ci config only shows when being triggered by a Git tag, i.e., a GitLab release). A release can be created by launching the last step (`manual-create-release`) in a commit pipeline - that would give us a new release with the ugly name containing the date and commit sha in the patch part (in accord to `semver` scheme):

![a link](pics/app_gitlab_release.png)

On the `Tags` tab for the same app you now can see a deployment part of the pipeline has been triggered by the just created GitLab release but no actual environments to deploy are displayed (the `_` item in the `Deploy-nonprod` stage is not an env):

![a link](pics/absent_envs.png)

So, now it's time to create an environment!

But before that we have to briefly switch to another team who are responsible for preparing infrastructure IaC templates. Navigate to the `infra/environment-blueprints` project and pretend you are a member of that team doing their job. Namely, imagine you have just created a some initial set of IaC files (they are already kindly prepared by me and present in the repository), tested them and now you feel that it's ready to be used by the other members of the project. You indicate such a readiness of a particular version of the IaC files by giving it a GitTag. Lets put a tag like `v1.0.0` onto the HEAD version.
You will see how the tags are going to be used just immediately. But first lets make some changes to the IaC files (e.g., add a new resource for some of the apps) and create a second Git tag, lets say `v1.1.0`. So, at this moment we have two versions of IaC templates (or `blueprints`) for our infrastructure - `v1.0.0` and `v1.1.0`.

Now we can return back to Jonh and his team. We assume Jonh is somehow informed that the version of the IaC templates he should use is `v1.0.0`. He wants to create a new preview environment out of the IaC templates of that version and put app1 and app2 into that env. 

(Here starts a description of how a user interoperates with the `infrastructure-set` Git repo. Notice that though the eventual idea is that it should be a Merge Request workflow (where you first get a Terraform plan within a Merge Request and can apply such a plan by merging the MR) widely advocated by GitLab but for the sake of simplicity here MR workflow is not implemented and instead direct push commits into a branch are made).

John wants the env be named `preview-for-johns-team`. He creates a new branch in the `infrastructure-set` repo with such a name and puts two files into it - a `version.txt` containing text `v1.0.0` and `apps.txt` with text `app1 app2` inside (the files format and its content is utterly simplified). 

The `infrastructure-set` pipeline is triggered by the new branch and first generates a Terraform plan using the set of the Terraform files indicated by the tag specified in `version.txt`. John reviews the plan and wants to proceed with creating the environment by starting the `Terraform-apply` stage:
![a link](pics/new_env_pipeline.png)

(To store Terraform plan as artifact and Terraform state the embeded features of the GitLab are leveraged - [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) and [Terraform HTTP backend by GitLab](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html))

Now return to the `app1` project and rerun the pipeline for the app1's release we created previously to make it regenerate a list of environments to deploy. You should see that the `preview-for-johns-team` item has appeared in the list of the environments:

![link](pics/new_env_in_the_deploy_pipeline.png)

Click the arrow button to deploy. Then refer to the `Deployments/Environments` section of the `app1` project to ensure a new env with the app1's release deployed into it is displayed.

We have successfuly created a new environment and deployed one of the apps into it!

Notice that albeit the above describes how users manually deploy the applications into an environment after it has been created which doesnt look really convinient, in a real life scenario we most likely would have some additional step in the `infrastructure-set` pipeline that runs after Terraform successfully finishes creating an environment and triggers deployment pipelines for all the applications specified in the `apps.txt` (and in that situation we would need to establish which versions of the applications should be deployed in such an automated manner - for example, those might be the latest versions available for each app or the versions currently deployed to production, etc).

Finally lets look at the case of updating an environment's infrastrure.

John got notified that a new version of the infrastructure templates is available (you remember that `v1.1.0` tag in the `environment-blueprints` repo ?). His team wants to asses how app1 would work within new conditions. They decide to update an existing env, namely `preview-for-johns-team`, for that purpose. 

John walks to the `preview-for-johns-team` branch of the `environment-set` repo and changes `version.txt`'s content from `v1.0.0` to `v1.1.0`. The branch pipeline gets triggered and first shows John a Terraform plan for a diff comparing to the current state of the environment and upon reviewing and accepting that diff John proceeds with actual updating the environment by launching `Terraform-apply` stage. That's it!

# Analysis of the advantages and disadvantages of the proposed solution

### Virtues

Given that this case assumes migrating from some existing ci/cd infrastructure based on Atlassian Bamboo with a lot of users used to it the proposed solution leverages the native capabilities of GitLab so that it mostly keeps the concepts and workflows used with Bamboo thus making the process of migration more smooth for the users.

The solution sticks to the GitOps tenets and empowers a project with all the virtues provided by Git. For example, it's usually easy to track any changes in the infrastructure back to Git repos. (May be not so easy for the `environment-set` project where we do not have the changes of the infrastructure captured in Git commits but in that case a task of finding differences between two states of a particular environment can be acomplished by fetching the two versions of the `environment-blueprints` repo corresponding to those states denoted in the `version.txt` and figuring out the differencies by using any apt tool).

The solution tends to stimulate in the users a self-service attitude to the infrastructure where most of the tasks of changing the infrastructure can be performed by users only familiar with basics of Git and Terraform. Thus it offloads DevOps team from some part of toil, removes dependence of a project members on Ops department which comes really handy especially for a large-scale projects.

### Shortcomings:

Besides the explicitly mentioned shortcomings which inherently stem from the necessity to utterly simplify all the aspects of this demo to make it comprehensible and possible to prepare in a sensible amount of time, this solution posseses some shortcomings that have to be resolved by using external tools to make this solution appropriate for a real life usage.

For examle, there is no way to have at some central dashboard an aggregated view of all the environments with all the apps and their versions deployed into the envs. This would require creating some custom SPA web app which would gather information from GitLab via API.







