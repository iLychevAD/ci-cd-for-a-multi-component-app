// Upload an avatar picture to a GitLab project or group using curl

// TODO: move avatars directory into the module directory

// TODO: somehow ensure that curl binary is available ?

variable entity_id {
  description = "GitLab group or project id"
}
variable entity_type {
  description = "Is a GitLab group or project ?" # TODO: somehow detect it automatically ?
  validation {
    condition     = contains(["group", "project"], var.entity_type)
    error_message = "Must be one of 'group', 'project'."
  }
}
variable avatar_filename {}

locals {
  avatar_filename = fileexists("${path.root}/avatars/${var.avatar_filename}") ? var.avatar_filename : "git.png"
  curl_result = join("", ["-w 'CURL result: ", "%", "{" ,"http_code", "}\n' "])
  upload_avatar_cmd = join("",
    [ "echo '##################### uploading ${local.avatar_filename} #####################' && ",
      "curl -s -o /dev/null ${local.curl_result} -XPUT ",
      " --header \"PRIVATE-TOKEN: $TF_VAR_gitlab_token\" ",
      "`echo -n $GITLAB_BASE_URL`",
      "${var.entity_type}s/${var.entity_id} ",
      "--form \"avatar=@avatars/${local.avatar_filename}\""
    ]
  )
}
resource "null_resource" "set_project_or_group_avatar" {
  provisioner "local-exec" {
    command = local.upload_avatar_cmd
    interpreter = ["bash", "-c"]
  }
}