terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.9.0"
    }
  }
}

variable project_name {}
variable group_id {}
variable description {
  default = ""
}
variable ci_config_path {
  default = ""
}

resource "gitlab_project" "_" {
    name = var.project_name
    namespace_id = var.group_id
    description = var.description
    ci_config_path = var.ci_config_path
    shared_runners_enabled = true
    visibility_level = "private"
}
resource "gitlab_deploy_key" "_" {
  project = gitlab_project._.path_with_namespace
  title   = "Terrafrom Provisioner readwrite"
  can_push = true
  key     = file(var.deploy_key_readwrite)

  provisioner "local-exec" {
    command = join("",
    [
      "echo Restore repo content on CREATE; ",
      "[ -d ${split("/", self.project)[length(split("/", self.project))-1]} ] ",
      "&& { ",
      "cd ${split("/", self.project)[length(split("/", self.project))-1]} && ",
      "git config --global --add safe.directory `pwd` && ",
      "git config --global init.defaultBranch main && ",
      "git init && ",
      "git config user.name demo && ",
      "git config user.email demo@example.com && ",
      "( git remote add origin 'git@'$GITLAB_SSH_URL':${self.project}.git' || true ) && ",
      "git remote set-url origin 'git@'$GITLAB_SSH_URL':${self.project}.git' && ",
      "git add . && ",
      "git commit -m 'Initial commit' && ",
      "GIT_SSH_COMMAND='ssh -i ${split("@", self.key)[1]} -o StrictHostKeyChecking=no' git push --force --all -u origin && ",
      "GIT_SSH_COMMAND='ssh -i ${split("@", self.key)[1]} -o StrictHostKeyChecking=no' git push --force --tags ",
      ";} || { echo Restoring repo from dir '${split("/", self.project)[length(split("/", self.project))-1]}' failed!;}"
    ]
  )
    working_dir = "${path.root}/repo_content"
  }

  provisioner "local-exec" {
    when    = destroy
    /* Note that for split() below to work we need to put the filename of the key inside itself after '@' */
    command = join("",
    [ "echo "
      # "echo Pull repo content on DESTROY; ",
      # "if [ -d ${split("/", self.project)[length(split("/", self.project))-1]} ]; then ",
      # "cd ${split("/", self.project)[length(split("/", self.project))-1]} && ",
      # "( git remote add origin 'git@gitlab.com:${self.project}.git' || true ) && ",
      # "git remote set-url origin 'git@gitlab.com:${self.project}.git' && ",
      # "GIT_SSH_COMMAND='ssh -i ${split("@", self.key)[1]} -o StrictHostKeyChecking=no' git pull --force -a --tags ",
      # "; else ",
      # "mkdir -p ${split("/", self.project)[length(split("/", self.project))-1]} && cd ${split("/", self.project)[length(split("/", self.project))-1]} && ",
      # "GIT_SSH_COMMAND='ssh -i ${split("@", self.key)[1]} -o StrictHostKeyChecking=no' ",
      # "git clone 'git@gitlab.com:${self.project}.git' . && ",
      # "GIT_SSH_COMMAND='ssh -i ${split("@", self.key)[1]} -o StrictHostKeyChecking=no' git pull --force -a --tags ",
      # "; fi"
    ]
  )
    working_dir = "${path.root}/repo_content"
  }
}

output "ssh_path" {
  value = gitlab_project._.ssh_url_to_repo
}
output "path_with_namespace" {
  value = gitlab_project._.path_with_namespace
}
output "id" {
  value = gitlab_project._.id
}
