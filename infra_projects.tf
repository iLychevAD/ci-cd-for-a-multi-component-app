variable "infra_projects_list" {
  description = "Infrastructure-related repositories"
  type = list(map(string))
  default = [
      {
          name = "environment-blueprints"
          description = "Templated blueprints aimed to be used for creating new environments"
      },
      {
          name = "environment-set"
          description = "Defines environment settings. Refer to this repo when you need to create/update/delete an environment",
          ci_config_name = "infra/environment-set.yml"
      },
      {
          name = "k8s-gitops"
          description = "Put apps manifests in here. Watched by a GitOps tool, installed into the k8s cluster"
      }
  ]
}

module "infra_project" {
  for_each = {for project in var.infra_projects_list: project.name => project }
  source = "./tf_modules/gitlab_project_with_restore_backup"

  gitlab_token = var.gitlab_token

  project_name = each.value.name
  group_id = gitlab_group.infra.id
  description = try(each.value.description, each.value.name)
  ci_config_path = try(each.value.ci_config_name, "") != "" ? "${each.value.ci_config_name}@${data.gitlab_group.root-gitlab-group.name}/ci/library:main" : ""

  deploy_key_readonly = var.deploy_key_readonly
  deploy_key_readwrite = var.deploy_key_readwrite

}

module "set_infra_projects_avatar" {
  for_each = {for project in var.infra_projects_list: project.name => project }
  depends_on = [ module.infra_project ]
  source = "./tf_modules/upload_avatar"

  entity_id = module.infra_project[each.value.name].id
  entity_type = "project"
  avatar_filename = "${each.value.name}.png"
}

#########################################################################################
# ci `environment-set` needs to be able to commit k9s config files into `k8s-gitops` repo
resource "tls_private_key" "ssh-key" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}
# "environment-set" holds the private part of the keypair...
resource "gitlab_project_variable" "K8S_GITOPS_PROJECT_DEPLOY_KEY_BASE64" {
  project   = module.infra_project["environment-set"].id
  key       = "K8S_GITOPS_PROJECT_DEPLOY_KEY_BASE64"
  value     = base64encode(tls_private_key.ssh-key.private_key_pem)
  masked = true
}
# ...and the public part is set as deploy key in "k8s-gitops"
resource "gitlab_deploy_key" "environment-set-project" {
  project = module.infra_project["k8s-gitops"].path_with_namespace
  title   = "environment-set project readwrite"
  can_push = true
  key     = tls_private_key.ssh-key.public_key_openssh
}
#########################################################################################

#########################################################################################
# Allow environment-set project to read environment-blueprints using deploy token
resource "gitlab_deploy_token" "environment-blueprints-read-repository" {
  project    = module.infra_project["environment-blueprints"].id
  name       = "environment-blueprints-read-repository"
  username   = "environment-blueprints-read-repository"
  expires_at = "2022-04-01T04:22:17Z"
  scopes = ["read_repository"]
}
# Write the token to environment-set project variables
resource "gitlab_project_variable" "ENVIRONMENT_BLUEPRINTS_DEPLOY_TOKEN" {
  project             =  module.infra_project["environment-set"].id
  key               = "ENVIRONMENT_BLUEPRINTS_DEPLOY_TOKEN"
  value             = gitlab_deploy_token.environment-blueprints-read-repository.token
  masked = true
}
#########################################################################################
