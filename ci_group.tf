resource "gitlab_group" "ci" {
  name        = "ci"
  path        = "ci"
  description = "CI related stuff: GitLab shared CI configs, OCI registry, etc"
  visibility_level = "private"

  parent_id = data.gitlab_group.root-gitlab-group.id
}
module "set_ci_group_avatar" {
  source = "./tf_modules/upload_avatar"

  entity_id = gitlab_group.ci.id
  entity_type = "group"
  avatar_filename = "ci.png"
}

output "ci_group_details" {
  value = join("",
      [
        "id: ${gitlab_group.ci.id}, ",
        "path: ${gitlab_group.ci.full_path}, ",
        "web_url: ${gitlab_group.ci.web_url}"
      ]
  )
}