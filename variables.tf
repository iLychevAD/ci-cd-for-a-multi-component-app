variable "gitlab_token" {}

variable "deploy_key_readonly" {}

variable "deploy_key_readwrite" {}

variable "root_gitlab_group_id" {}

